#!/bin/bash
docker run -d --name avalier.meteor.demo \
  -e ROOT_URL=http://localhost:3000 \
  -e MONGO_URL=mongodb://url \
  -e MONGO_OPLOG_URL=mongodb://oplog_url \
  -e MAIL_URL=smtp://mail_url.com \
  -p 3000:3000 \
  avalier/meteor-demo:0.1

